# Uncomment if you want to run tests in transaction mode with a final rollback
#from django.test import TestCase
#uncomment this if you want to keep data after running tests
from unittest import TestCase
from django.core.urlresolvers import reverse
from django.contrib.staticfiles import finders
from django.contrib.auth.models import User
from models import UserProfile
from forms import  UserForm
from forms import  UserProfileForm
from django.core.files.uploadedfile import SimpleUploadedFile
from PIL import Image
from StringIO import StringIO # Python 3: from io import StringIO
from django.core.files.uploadedfile import InMemoryUploadedFile
from os.path import basename, join
from django.conf import settings
from django.test import Client
from rango.models import Category

#python ./manage.py test rango.tests.UserAuthenticationTests --keepdb
#class UserAuthenticationTests(TestCase):

username = "k"
passwd="p"
email="kk@pp.es"
website="http://elpais.es"
picture = "black.jpg"

def populate():
    """  auxiliary function to populate database
    """
    try:
        from populate_rango import populate
        populate()
    except ImportError:
        print('The module populate_rango does not exist')
    except NameError:
        print('The function populate() does not exist or is not correct')
    except:
        print('Something went wrong in the populate() function :-(')

def createPicture():
    im = Image.new(mode='RGB', size=(200, 200)) # create a new image using PIL
    im_io = StringIO() # a StringIO object for saving image
    im.save(im_io, 'JPEG') # save the image to im_io
    im_io.seek(0) # seek to the beginning

    image = InMemoryUploadedFile(
        im_io, None, picture, 'image/jpeg', im_io.len, None
        )
    return image

class UserAuthenticationTests(TestCase):
    def setUp(self):
        self.client = Client()

    def test_index_page(self):
        """check that default page with login return 'Rango says username'
           and without login 'Rango says ... hello world'"""

        #check if user exists. if not create one
        client = Client()
        try:
            userKK = User.objects.get(username=username)
        except User.DoesNotExist:
            user = User(username=username, email=email,password=passwd)
            user.save()

        #logout (just in case) -> logout redirects to index by
        # I will call it explicitely
        response = self.client.get(reverse('logout'), follow=True)
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('index'), follow=True)

        #test message
        self.assertEqual(response.status_code, 200)#redirection
        self.assertIn(b'Rango says... hello world!', response.content)
        self.assertNotIn(b'Logout', response.content)
        self.assertNotIn(b'Add a New Category', response.content)
        self.assertIn(b'Login', response.content)

        #login
        loginDict={}
        loginDict["username"]=username
        loginDict["password"]=passwd
        response = self.client.post(reverse('login'), loginDict, follow=True)#follow redirection
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('index'), follow=True)

        #test message
        message = b"Rango says... hello %s!"%username
        self.assertIn(message, response.content)
        self.assertIn(b'Logout', response.content)
        self.assertNotIn(b'Login', response.content)
        self.assertIn(b'Add a New Category', response.content)


    def test_create_user_using_view(self):
        """  test user creation functions in views.py (no form used)"""
        #delete user if it exists
        try:
            userKK = User.objects.get(username=username)
            userKK.delete()
        except User.DoesNotExist:
            pass
        #create user self.username with password self.passwd
        #fill user form
        loginDict={}
        loginDict["username"]=username
        loginDict["email"]=email
        loginDict["password"]=passwd
        userForm = UserForm(data=loginDict)

        #fill profile form
        profileDict={}
        profileDict['website']=website
        #create auxiliary black image for profileForm
        filesDict={'picture' : createPicture()}
        userProfileForm = UserProfileForm(data=profileDict, files=filesDict)

        if userForm.is_valid() and userProfileForm.is_valid():
            try:
                # Save the user's form data to the database.
                user = userForm.save()
                # Now we hash the password with the set_password method.
                # Once hashed, we can update the user object.
                user.set_password(user.password)
                user.save()
                #we need to set a user object in userprofile
                userprofile = userProfileForm.save(commit=False)
                userprofile.user = user
                userprofile.save()

            except Exception as e:
                raise Exception("Error processing form: %s"%e.message)

        self.assertTrue(userForm.is_valid())
        self.assertTrue(userProfileForm.is_valid())

        #check user name
        try:
            userKK = User.objects.get(username=username)
        except User.DoesNotExist:
            userKK=User()
        self.assertEqual(userKK.email,email)

        #check file name
        try:
            userprofileKK = UserProfile.objects.get(user=userKK)
        except User.DoesNotExist:
            userprofileKK=UserProfile()
        imageName = join(settings.MEDIA_ROOT, userprofileKK.picture.name)
        with Image.open(imageName) as im:
            width, height = im.size
        self.assertEqual(width,200)

    def test_register_page(self):
        """  test user creation functions using web form"""
        #delete user if it exists
        try:
            userKK = User.objects.get(username=username)
            userKK.delete()
        except User.DoesNotExist:
            pass

        loginDict={}
        loginDict["username"]=username
        loginDict["email"]=email
        loginDict["password"]=passwd
        loginDict['website']=website
        loginDict['picture']=createPicture()

        response = self.client.post(reverse('register'), loginDict, follow=True)#follow redirection
        self.assertEqual(response.status_code, 200)#redirection
        userKK = User.objects.get(username=username)
        self.assertEqual(username,userKK.username)
        userProfileKK = UserProfile.objects.get(user=userKK)
        self.assertEqual(website,userProfileKK.website)
        imageName = join(settings.MEDIA_ROOT, userProfileKK.picture.name)
        with Image.open(imageName) as im:
            width, height = im.size
        self.assertEqual(width,200)

    def test_category_page(self):
        """check that  category page with no login does not allow to add pages'"""
        #populate database (in case is empty)
        populate()

        #logout (in case we are logged in)
        response = self.client.get(reverse('logout'), follow=True)
        self.assertEqual(response.status_code, 200)#redirection

        #connect category/python page (no add_page available)
        #see urls.py to understand kwargs values
        response = self.client.get(reverse('show_category',kwargs={'category_name_slug':'python'}))
        self.assertIn(b'Official Python Tutorial', response.content)
        self.assertNotIn(b'add_page', response.content)

        #login
        loginDict={}
        loginDict["username"]=username
        loginDict["password"]=passwd
        response = self.client.post(reverse('login'), loginDict, follow=True)#follow redirection
        self.assertEqual(response.status_code, 200)#redirection
        #connect category/python page (now add_page is available)
        response = self.client.get(reverse('show_category',kwargs={'category_name_slug':'python'}))
        self.assertIn(b'Official Python Tutorial', response.content)
        self.assertIn(b'add_page', response.content)




from django.test import TestCase
class GeneralTests(TestCase):
    def test_serving_static_files(self):
        # If using static media properly result is not NONE once it finds rango.jpg
        result = finders.find('images/rango.jpg')
        self.assertIsNotNone(result)



class IndexPageTests(TestCase):

    def setUp(self):
        self.client = Client()

    def test_index_contains_hello_message(self):
        # Check if there is the message 'Rango Says'
        # Chapter 4
        response = self.client.get(reverse('index'))
        self.assertIn(b'Rango says', response.content)

    def test_index_using_template(self):
        # Check the template used to render index page
        # Chapter 4
        response = self.client.get(reverse('index'))
        self.assertTemplateUsed(response, 'rango/index.html')

    def test_rango_picture_displayed(self):
        # Check if is there an image called 'rango.jpg' on the index page
        # Chapter 4
        response = self.client.get(reverse('index'))
        self.assertIn(b'img src="/static/images/rango.jpg', response.content)

    def test_index_has_title(self):
        # Check to make sure that the title tag has been used
        # And that the template contains the HTML from Chapter 4
        response = self.client.get(reverse('index'))
        self.assertIn(b'<title>', response.content)
        self.assertIn(b'</title>', response.content)


class AboutPageTests(TestCase):

    def setUp(self):
        self.client = Client()

    def test_about_contains_create_message(self):
        # Check if in the about page is there - and contains the specified message
        # Exercise from Chapter 4
        response = self.client.get(reverse('about'))
        self.assertIn(b'This tutorial has been put together by', response.content)
        
        
    def test_about_contain_image(self):
        # Check if is there an image on the about page
        # Chapter 4
        response = self.client.get(reverse('about'))
        self.assertIn(b'img src="/static/images/', response.content)
        
    def test_about_using_template(self):
        # Check the template used to render index page
        # Exercise from Chapter 4 
        response = self.client.get(reverse('about'))

        self.assertTemplateUsed(response, 'rango/about.html')
        
        
        
class ModelTests(TestCase):

    def setUp(self):
        populate()
        
        
    def get_category(self, name):
        

        try:                  
            cat = Category.objects.get(name=name)
        except Category.DoesNotExist:    
            cat = None
        return cat
        
    def test_python_cat_added(self):
        cat = self.get_category('Python')  
        self.assertIsNotNone(cat)
         
    def test_python_cat_with_views(self):
        cat = self.get_category('Python')
        self.assertEquals(cat.views, 128)
        
    def test_python_cat_with_likes(self):
        cat = self.get_category('Python')
        self.assertEquals(cat.likes, 64)
        
        
